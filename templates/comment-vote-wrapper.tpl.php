<div class="comment-vote-wrapper">
  <div class="vote-up-wrapper">
  <?php print render($vote_up_link);?>
  <span class="vote-green vote-up-<?php print render($cid)?>"><?php print render($vote_up)?></span>
  </div>
  <div class="vote-down-wrapper">
  <?php print render($vote_down_link);?>
  <span class="vote-red vote-down-<?php print render($cid)?>"><?php print render($vote_down)?></span>
  </div>
</div>