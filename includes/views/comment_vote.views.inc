<?php
/**
 * @file
 * Provide views data and handlers for comment_vote.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function comment_vote_views_data_alter(&$data) {
  // Number of votes.
  $data['comment']['vote_total'] = array(
    'title' => t('Total votes'),
    'help' => t('Number of total votes comment.'),
    'field' => array(
      'handler' => 'comment_vote_views_handler_field_vote_total',
    ),
    'sort' => array(
      'handler' => 'comment_vote_views_handler_sort_vote_total',
    ),
  );

  // Number of positive votes.
  $data['comment']['vote_up'] = array(
    'title' => t('Positive votes'),
    'help' => t('Number of positive votes on comment.'),
    'field' => array(
      'handler' => 'comment_vote_views_handler_field_vote_up',
    ),
    'sort' => array(
      'handler' => 'comment_vote_views_handler_sort_vote_up',
    ),
  );

  // Number of negative votes.
  $data['comment']['vote_down'] = array(
    'title' => t('Negative votes'),
    'help' => t('Number of negative votes on comment.'),
    'field' => array(
      'handler' => 'comment_vote_views_handler_field_vote_down',
    ),
    'sort' => array(
      'handler' => 'comment_vote_views_handler_sort_vote_down',
    ),
  );

  // Preview of votes on comment.
  $data['comment']['full_vote'] = array(
    'title' => t('Display and Vote'),
    'help' => t('Display votes with possibility to vote.'),
    'field' => array(
      'handler' => 'comment_vote_views_handler_field_vote_full',
    ),
  );
}

