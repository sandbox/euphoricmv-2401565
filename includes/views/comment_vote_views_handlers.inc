<?php

/**
 * @file
 * Provide field and sort handlers for comment_vote.module.
 */

/**
 * Sort comments by total votes.
 *
 * @ingroup views_field_handlers
 */
class comment_vote_views_handler_sort_vote_total extends views_handler_sort {
  function query() {
    $this->ensure_my_table();
    $this->query->add_field(NULL, $this->table_alias . '.vote_up + ' . $this->table_alias . '.vote_down', $this->real_field);
    $this->query->add_orderby(NULL, NULL, $this->options['order'], $this->real_field);
  }
}

/**
 * Sort comments by positive votes.
 *
 * @ingroup views_field_handlers
 */
class comment_vote_views_handler_sort_vote_up extends views_handler_sort {
  function query() {
    $this->ensure_my_table();
    $this->query->add_field($this->table_alias, $this->real_field, $this->field);
    $this->query->add_orderby($this->table_alias, $this->real_field, $this->options['order'], $this->field);
  }
}

/**
 * Sort comments by negative votes.
 *
 * @ingroup views_field_handlers
 */
class comment_vote_views_handler_sort_vote_down extends views_handler_sort {
  function query() {
    $this->ensure_my_table();
    $this->query->add_field($this->table_alias, $this->real_field, $this->field);
    $this->query->add_orderby($this->table_alias, $this->real_field, $this->options['order'], $this->field);
  }
}

/**
 * Field handler to present a number of total votes on comments.
 *
 * @ingroup views_field_handlers
 */
class comment_vote_views_handler_field_vote_total extends views_handler_field_entity {

  function query($group_by = FALSE) {
    $this->ensure_my_table();
    $this->query->add_field(NULL, $this->table_alias . '.vote_up + ' . $this->table_alias . '.vote_down', $this->real_field);
  }

  function render($values) {
    // Otherwise return empty string
    return $values->{$this->real_field};
  }

}

/**
 * Field handler to present a number of positive votes on comments.
 *
 * @ingroup views_field_handlers
 */
class comment_vote_views_handler_field_vote_up extends views_handler_field_entity {

  function query($group_by = FALSE) {
    $this->ensure_my_table();
    $this->query->add_field($this->table_alias, $this->real_field, $this->field);
  }

  function render($values) {
    // Otherwise return empty string
    return $values->{$this->real_field};
  }

}

/**
 * Field handler to present a number of positive votes on comments.
 *
 * @ingroup views_field_handlers
 */
class comment_vote_views_handler_field_vote_down extends views_handler_field_entity {

  function query($group_by = FALSE) {
    $this->ensure_my_table();
    $this->query->add_field($this->table_alias, $this->real_field, $this->field);
  }

  function render($values) {
    // Otherwise return empty string
    return $values->{$this->real_field};
  }

}

/**
 * Field handler to present a number of votes on comments and give posibility to vote.
 *
 * @ingroup views_field_handlers
 */
class comment_vote_views_handler_field_vote_full extends views_handler_field_entity {

  function render($values) {

    $current_path = current_path();
    $path = drupal_get_path_alias($current_path);

    global $user;

    // Get comment object.
    $comment = $this->get_value($values);

    return _comment_vote_full_display($comment->cid, $user->uid, ip_address(), $comment->vote_up, $comment->vote_down, $path);
  }

}
