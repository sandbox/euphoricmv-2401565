<?php

/**
 * Implements hook_preprocess_comment().
 */
function comment_vote_preprocess_comment(&$variables) {
  // Get comment object from variables.
  $comment = $variables['elements']['#comment'];

  // Get path.
  $current_path = current_path();
  $path = drupal_get_path_alias($current_path);

  // Add new variables for voting.
  $variables['vote_up'] = $comment->vote_up;
  $variables['vote_down'] = $comment->vote_down;

  // Render full mode for votes.
  $variables['content']['comment_votes'][0]['#markup'] = _comment_vote_full_display($comment->cid, $variables['user']->uid, ip_address(), $variables['vote_up'], $variables['vote_down'], $path);
  $variables['content']['comment_votes']['#weight'] = 100;
}

/**
 * Implements hook_menu().
 */
function comment_vote_menu() {
  $items = array();

  $items['comment-voteup/%'] = array(
    'title' => 'Comment vote',
    'description' => 'Get comment infos',
    'page callback' => 'comment_vote_up',
    'page arguments' => array(1),
    'access arguments' => array('access content'),
  );
  $items['comment-votedown/%'] = array(
    'title' => 'Comment vote',
    'description' => 'Get comment infos',
    'page callback' => 'comment_vote_down',
    'page arguments' => array(1),
    'access arguments' => array('access content'),
  );

  return $items;
}

/**
 * Comment vote up
 */
function comment_vote_up($cid) {
  global $user, $base_url;

  // Load all neccessery fields for comments from db.
  $queryc = db_select('comment', 'cmnt');
  $queryc->fields('cmnt', array('uid', 'cid', 'vote_up', 'vote_down'));
  $queryc->condition('cmnt.cid', $cid);
  $result = $queryc->execute()->fetchAssoc();
  $cmnt_uid = $result['uid'];
  $cmnt_vote_up = $result['vote_up'];

  // Check if user already voted on this comment.
  $query = db_select('comment_votes', 'scv');
  $query->fields('scv', array());
  $query->condition('scv.uid', $cmnt_uid);
  $query->condition('scv.cid', $cid);
  $query->condition('scv.ip_address', ip_address());
  $rows = $query->execute();
  $count = $rows->rowCount();

  if ($count == 0) {
    // Insert comment vote record in comment_votes taqble.
    $comment_votes = db_insert('comment_votes')
      ->fields(array(
        'cid' => $cid,
        'uid' => $user->uid,
        'ip_address' => ip_address(),
      ))
      ->execute();

    // Increment vote up and write it to the comment table.
    $cmnt_vote_up_inc = $cmnt_vote_up + 1;

    // Update comment table with vote.
    db_update('comment')
      ->condition('cid', $cid)
      ->fields(array('vote_up' => $cmnt_vote_up_inc))
      ->execute();

    // Clear cache for this comment.
    cache_clear_all($cid, 'cache_entity_comment');
    if (isset($_GET['destination'])) {
      $destination = $base_url . '/' . $_GET['destination'];
      cache_clear_all($destination, 'cache_page');
    }

    // Output json object with parameters.
    return drupal_json_output(array(
      'num_votes' => $cmnt_vote_up_inc,
      'cid' => $cid
    ));
    drupal_exit();
  }
  else {
    return drupal_json_output(array());
    drupal_exit();
  }
}

/**
 * Comment vote down
 */
function comment_vote_down($cid) {
  global $user, $base_url;

  // Load all neccessery fields for comments from db.
  $queryc = db_select('comment', 'cmnt');
  $queryc->fields('cmnt', array('uid', 'cid', 'vote_up', 'vote_down'));
  $queryc->condition('cmnt.cid', $cid);
  $result = $queryc->execute()->fetchAssoc();
  $cmnt_uid = $result['uid'];
  $cmnt_vote_down = $result['vote_down'];

  // Check if user already voted on this comment.
  $query = db_select('comment_votes', 'scv');
  $query->fields('scv', array());
  $query->condition('scv.uid', $cmnt_uid);
  $query->condition('scv.cid', $cid);
  $query->condition('scv.ip_address', ip_address());
  $rows = $query->execute();
  $count = $rows->rowCount();

  if ($count == 0) {
    // Insert comment vote record in comment_votes taqble.
    $comment_votes = db_insert('comment_votes')
      ->fields(array(
        'cid' => $cid,
        'uid' => $user->uid,
        'ip_address' => ip_address(),
      ))
      ->execute();

    // Increment vote up and write it to the comment table.
    $cmnt_vote_down_inc = $cmnt_vote_down + 1;

    // Update comment table with vote.
    db_update('comment')
      ->condition('cid', $cid)
      ->fields(array('vote_down' => $cmnt_vote_down_inc))
      ->execute();

    // Clear cache for this comment.
    cache_clear_all($cid, 'cache_entity_comment');
    if (isset($_GET['destination'])) {
      $destination = $base_url . '/' . $_GET['destination'];
      cache_clear_all($destination, 'cache_page');
    }

    // Output json object with parameters.
    return drupal_json_output(array(
      'num_votes' => $cmnt_vote_down_inc,
      'cid' => $cid
    ));


    drupal_exit();
  }
  else {
    return drupal_json_output(array());
    drupal_exit();
  }
}

/**
 * Implements hook_theme().
 */
function comment_vote_theme() {
  return array(
    'comment_vote_wrapper' => array(
      'arguments' => array(),
      'template' => 'templates/comment-vote-wrapper',
    ),
    'comment_vote_mini_wrapper' => array(
      'arguments' => array(),
      'template' => 'templates/comment-vote-mini-wrapper',
    ),
  );
}

/**
 * Implements hook_views_api().
 */
function comment_vote_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'comment_vote') . '/includes/views',
  );
}

/**
 * Render comment voting template.
 */
function _comment_vote_full_display($cid, $uid, $ip_address, $vote_up, $vote_down, $path) {
  // Parameters for template.
  $output = array();
  $output['cid']['#markup'] = $cid;
  $output['uid']['#markup'] = $uid;
  $output['vote_up']['#markup'] = $vote_up;
  $output['vote_down']['#markup'] = $vote_down;

  // Check if user already voted on this comment.
  $query = db_select('comment_votes', 'scv');
  $query->fields('scv', array());
  $query->condition('scv.uid', $uid);
  $query->condition('scv.cid', $cid);
  $query->condition('scv.ip_address', $ip_address);
  $result = $query->execute();
  $count = $result->rowCount();


  if ($count == 0) {
    // Render Vote up Link.
    $vote_up_link = theme('html_tag', array(
      'element' => array(
        '#tag' => 'span',
        '#attributes' => array(
          'class' => array('voting-link', 'vote-up'),
          'data-href' => '/comment-voteup/' . $cid,
          'data-destination' => $path
        ),
        '#value' => t('Vote up'),
      ),
    ));

    // Render Vote up Link.
    $vote_down_link = theme('html_tag', array(
      'element' => array(
        '#tag' => 'span',
        '#attributes' => array(
          'class' => array('voting-link', 'vote-down'),
          'data-href' => '/comment-votedown/' . $cid,
          'data-destination' => $path
        ),
        '#value' => t('Vote down'),
      ),
    ));
  }
  else {
    $vote_up_link = '<span class="disabled">' . t('Vote up') . '</span>';
    $vote_down_link = '<span class="disabled">' . t('Vote down') . '</span>';
  }

  $output['vote_up_link']['#markup'] = $vote_up_link;
  $output['vote_down_link']['#markup'] = $vote_down_link;

  return theme('comment_vote_wrapper', $output);
}