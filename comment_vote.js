/**
 * Function that automatically update number of reminders on the top
 */
(function ($) {
  Drupal.behaviors.comment_vote = {
    attach: function (context, settings) {

      $('.voting-link').once().click(function (e) {
        // Get all classes.
        var linkClasses = this.classList;

        // Get url vor voting.
        var href = $(this).attr('data-href');
        var destination = $(this).attr('data-destination');
        var fullHref = href + '?destination=' + destination;

        // Disable another vote.
        $('span[data-href="' + href + '"]').each(function (index, value) {
          var parentDiv = $(this).closest('.comment-vote-wrapper');
          var childLink = parentDiv.children().find('span.voting-link');
          childLink.replaceWith('<span class="disabled">' + $(this).text() + '</span>');
        });

        // Vote and insert new number of votes.
        $.get(fullHref, function (data) {
          var voteSpan = $("." + linkClasses[1] + '-' + data.cid);
          voteSpan.each(function (index, value) {
            $(this).html(data.num_votes);
          });
        });

      });
    }
  };
})(jQuery);